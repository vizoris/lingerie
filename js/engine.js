$(function() {


// ScrollBar
 new PerfectScrollbar('.scrollbar');
	

// Scroll Links
$(".scroll-block, .article-anchors").on("click","a", function (event) {
    //отменяем стандартную обработку нажатия по ссылке
    event.preventDefault();

    //забираем идентификатор бока с атрибута href
    var id  = $(this).attr('href'),

    //узнаем высоту от начала страницы до блока на который ссылается якорь
      top = $(id).offset().top;
    
    //анимируем переход на расстояние - top за 1500 мс
    $('body,html').animate({scrollTop: top - 50}, 1500);
  });


// Меню 
$('.drop > a').click(function() {
	$(this).parent('.drop').children('ul').fadeToggle();

})

$('.brands-menu >li').click(function() {


	if ($(this).children('a').hasClass('active')) {
		$(this).children('a').removeClass('active');
		$(this).children('ul').slideUp()
		console.log('yes active')
	}else {
		$('.brands-menu >li').children('ul').slideUp();
		$('.brands-menu >li').children('a').removeClass('active');
		$(this).children('ul').fadeIn();
		$(this).children('a').addClass('active');
	}
	


})

$('.product-slider').slick({
  dots: false,
  arrows: true,
  speed: 300,
  slidesToShow: 6,
  slidesToScroll: 1,
  // autoplay: true,
  // autoplaySpeed: 5000,
  responsive: [
    {
      breakpoint: 1199,
      settings: {
        slidesToShow: 4,
      }
    },
    {
      breakpoint: 991,
      settings: {
        slidesToShow: 3,
      }
    },
    {
      breakpoint: 550,
      settings: {
        slidesToShow: 2
      },
      
    },
    {
      breakpoint: 479,
      settings: {
        slidesToShow: 1
      },
      
    }
  ]
});


//isotope
$('.pins').imagesLoaded( function() {
        // BEGIN of script for isotope/filter
        // init Isotope
        var isotopeGutter = 20;

        var $projects = $('.pins').isotope({
            // options
            itemSelector: '[class^="col"]',
            masonry: {
                gutter: isotopeGutter
            },
        });
// filter items on button click
        $('.filters').on( 'click', 'a', function() {
            var filterValue = $(this).attr('data-filter');
            $projects.isotope({ filter: filterValue });
            $('.filters a').removeClass('active');
            $(this).addClass('active');
        });
// END of script for isotope/filter
    });




// MMENU
var menu = new MmenuLight(
  document.querySelector( '#mobile-menu' ),
  'all'
);

var navigator = menu.navigation({
  // selectedClass: 'Selected',
  // slidingSubmenus: true,
  // theme: 'dark',
   title: 'Меню'
});

var drawer = menu.offcanvas({
  // position: 'left'
});

//  Open the menu.
document.querySelector( 'a[href="#mobile-menu"]' )
  .addEventListener( 'click', evnt => {
    evnt.preventDefault();
    drawer.open();
});


// FansyBox
 $('.fancybox').fancybox({});








$('.mobile-header__sidebar').click(function() {
  
  $('.content-inner').toggleClass('active');
  $('.content-aside').toggleClass('active');



})








})